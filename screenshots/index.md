---
layout: default
title: OpenConnect VPN - Screenshots
---

## OpenConnect VPN for Windows screenshots (1.6.0)

### Application

![oc-gui-main_profiles](win_app_main.png)

![oc-gui-main_info](win_app_info.png)

![oc-gui-main_info](win_app_new.png)

![oc-gui-main_info](win_app_edit.png)

![oc-gui-main_connect](win_app_connect.png)

![oc-gui-systray](win_app_about.png)

![oc-gui-systray](win_app_systray.png)

![oc-gui-systray](win_app_log.png)

### Windows installer

![oc-gui-profiles_edit](win_installer_initial.png)

![oc-gui-profiles_edit](win_installer_choose.png)

