---
layout: post
title: Enabling donations for OpenConnect VPN GUI
subtitle: Make the project sustainable
cover-img: ["/assets/img/jakub-brabec-N9RnWgVprzQ-unsplash.jpg" : 'Photo by 🇨🇿 Jakub Brabec on Unsplash']
share-img: /assets/img/jakub-brabec-N9RnWgVprzQ-unsplash.jpg
tags: [opencollective, sponsors]
author: Nikos Mavrogiannopoulos
---

As we are enhancing our VPN client for Windows and MacOS we're embarking on a new experiment.
For the first time, we are <a class="plausible-event-name=Contribute" href="https://opencollective.com/openconnect-vpn/contribute">opening the project to donations</a>
under Open Source Collective. This decision marks a significant
step in our mission to ensure the project's sustainability and independence, and we're doing so
with a learning mindset, fully aware that this is an exploration into uncharted territory.

This initiative is to support our commitment to deliver an open source secure, standards
compliant VPN with enterprise VPN features. Whether you are an organization using openconnect
VPN or an end-user, your support will enable us to ensure the project’s sustainability,
and enable us to move towards a future where we have regular releases with security and
functionality updates, and new features.

We view this as a unique opportunity to learn and grow together. Your contribution,
no matter the size, will play a crucial role in shaping the future of our mission.

Thank you for your unwavering support and belief in our mission.

<a class="plausible-event-name=Contribute" href="https://opencollective.com/openconnect-vpn/contribute" target="_blank">
  <img src="https://opencollective.com/openconnect-vpn/contribute/button@2x.png?color=blue" width=300 />
</a>
