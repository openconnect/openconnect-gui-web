---
layout: post
title: OpenConnect 1.6.2 is digitally signed
cover-img: ["/assets/img/scott-graham-OQMZwNd3ThU-unsplash.jpg" : 'Photo by Scott Graham on Unsplash']
share-img: /assets/img/scott-graham-OQMZwNd3ThU-unsplash.jpg
tags: [openconnect, signatures, certum]
author: Nikos Mavrogiannopoulos
---

The OpenConnect VPN graphical client release of 1.6.2 is the first digitally signed
version with a code signing certificate. Due to the digital signature users
do not face big scary warnings from windows when attempting to download and
run the installer. The software is signed with an open source code signing
certificate issued by Certum that is part of the Microsoft Trusted Root Program.

Addressing the code signing issue was quite a challenge given that code
signing assumes a dedicated system with a smart card for that purpose, while
we operate on a distributed environment with code and build system on
GitLab. To address this we have developed the necessary infrastructure to
sign code as part of our CI process with p11-kit. Kudos go to
[Marios](https://gitlab.com/mpaouris).

This effort wouldn't have been possible without the sponsorship of
[OpusV](https://www.opusv.com.au/). You can make a difference for this project
[by joining our sponsors](https://opencollective.com/openconnect-vpn/contribute).

PS. As windows has an additional reputation-based checker for newly
installed software, a warning is still present until a certain threshold
of users downloads and uses the new version. 
