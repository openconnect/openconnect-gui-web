---
layout: post
title: OpenConnect is not affected by the xz backdoor
cover-img: ["/assets/img/davies-designs-studio-G-6kwVnClsE-unsplash.jpg" : 'Photo by Davies Designs Studio on Unsplash']
share-img: /assets/img/davies-designs-studio-G-6kwVnClsE-unsplash.jpg
tags: [openconnect, xz-backdoor, cve-2024-3094]
author: Nikos Mavrogiannopoulos
---

OpenConnect does not use LZMA for compression and is
**not** affected by the xz backdoor (CVE-2024-3094).

Visit [this technical page](https://ocserv.openconnect-vpn.net/technical.html)
for an in-depth description of openconnect protocol and implementation.
