all:
	mkdir -p download
	./generate-release-info.py > _data/release.yml
	sed -i 's|\#\([0-9]\+\)|[\#\1](https://gitlab.com/openconnect/openconnect-gui/issues/\1)|g' _data/release.yml
	bundle install
	bundle exec jekyll build -d public
	