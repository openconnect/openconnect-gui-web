# OpenConnect graphical client web pages
This repository contains the source code for the web pages at:
 * https://gui.openconnect-vpn.net

## Setup

To apply changes, fork this repository (or create a branch).
After forking the repo and updating the pages run:

- `make`
- `bundle exec jekyll serve`

## images/screenshots preparation:
    - prepare desired screenshots (common width is 650px)
    - create or upload images to e.g. screenshots folder
    - commit & submit - and continue with web-text changes...

## web page text:
    - Edit for index.md while using markdown
    - review your changes and commit

