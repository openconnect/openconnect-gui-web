---
layout: default
title: Download OpenConnect VPN for Windows
---

<div class="container"  style="margin-bottom: 5em;">
  <div class="row">
    <div class="col-md-6" style="display: flex; align-items: center;">
      <div>
        <h2>OpenConnect VPN for Windows</h2>
        <p>
        OpenConnect VPN graphical client is an open source Enterprise VPN client that provides security and privacy with seamless usability.
        </p>

        <div class="d-flex flex-column align-items-center">
          <a class="btn btn-primary btn-with-text-below" href="{{ site.data.release.url }}" role="button">Download</a>
          <p class="small-text-under-btn">
              Version {{ site.data.release.version }} for Windows 10 or later version<br/>
              Released on {{ site.data.release.date }}
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <img class="shadow" src="/screenshots/win_app.png" alt="OpenConnect VPN GUI" style="max-width:100%;">
    </div>
  </div>
</div>

## ChangeLog

{{ site.data.release.changelog }}

## Older releases

[See here for the older releases archive](https://gitlab.com/openconnect/openconnect-gui/-/releases).


