#!/usr/bin/env python3

import requests
import sys
import yaml
from datetime import datetime

def fetch_latest_release(project_id):
    """Fetch the latest release information for a GitLab project."""
    sys.stderr.write("Getting the latest release information...\n")
    api_url = f"https://gitlab.com/api/v4/projects/{project_id}/releases"
    response = requests.get(api_url)
    if response.status_code == 200 and response.json():
        latest_release = response.json()[0]  # Assume the first one is the latest
        sys.stderr.write(f"Latest release is {latest_release['tag_name']}.\n")
        return latest_release
    else:
        sys.stderr.write(f"Failed to fetch the latest release: {response.status_code}\n")
        return None

def generate_yaml(release, image):
    sys.stderr.write(f"Generating yaml for {release['tag_name']}...\n")
    time_obj = datetime.strptime(release['released_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
    release_time = time_obj.strftime('%B %d, %Y')
    data = {
        "version": release['name'],
        "url": release['assets']['links'][0]['url'],
        "date": release_time,
        "changelog": release['description']
    }

    yaml_str = yaml.dump(data, default_flow_style=False, allow_unicode=True, width=1000, indent=4)

    return yaml_str

def main():
    project_id = "12274423"  # Project ID for OpenConnect VPN GUI
    image = "/screenshots/win_app.png"
    latest_release = fetch_latest_release(project_id)
    if latest_release:
        yaml_content = generate_yaml(latest_release, image)
        sys.stderr.write("Markdown content generated successfully.\n")
        print(yaml_content)
        sys.exit(0)
    else:
        sys.stderr.write("Failed to generate yaml content.")
        sys.exit(1)

if __name__ == "__main__":
    main()
